var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var categoria_schema = new Schema({
	nombre: { type: String },
	descripcion: { type: String },
	restaurante: {type:Schema.Types.ObjectId,ref:"Restaurantes"}
});

module.exports = mongoose.model("Categorias",categoria_schema);