var router = require('express').Router();
var inventarioController = require('../controllers/inventarioController');
	
	//Save Inventario
	router.post('/', inventarioController.saveInventario);

	//Update Inventario
	router.put('/:id', inventarioController.updateInventario);

	//Delete Inventario
	router.delete('/:id', inventarioController.deleteInventario);

	//FindAll Inventario
	router.get('/', inventarioController.listInventario);

	//getOne Inventario
	router.get('/:id',inventarioController.getInventario);
	


module.exports = router;