var router=require('express').Router();
var extraController=require('../controllers/extraController');

//save
router.post('/',extraController.saveExtra);
//update
router.put('/:idExtra',extraController.updateExtra);
//delete
router.delete('/:idExtra',extraController.deleteExtra);
//getAll
router.get('/',extraController.listExtras);

module.exports=router;


