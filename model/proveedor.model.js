var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var proveedor_schema = new Schema({
    nombre: {type:String},
    telefono: {type:String},
    restaurante: {type:Schema.Types.ObjectId,ref:"Restaurantes"}
});

module.exports = mongoose.model("Proveedores", proveedor_schema);