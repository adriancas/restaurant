var router = require('express').Router();
var menuController=require('../controllers/menuController');

//save
router.post('/',menuController.saveMenu);
//update
router.put('/:idMenu',menuController.updateMenu);
//delete
router.delete('/:idMenu',menuController.deleteMenu);
//find
router.find('/',menuController.listMenu);

module.exports=router;