var Usuario = require('../../model/proveedor.model.js');
//var Restaurant = require('../../model/restaurant.model.js');


async function saveUser(req,res){
  try{
    var usuario=new Usuario({
      usuario:req.body.usuario,
      password:req.body.password,
      rol:req.body.rol,
      status:req.body.status,
      restaurante:req.usuario.restaurante
    });
    var saved = await usuario.save();
    res.json(saved);

  }catch(err){
    res.status(500).json({err:'No se guardo'});

  }
}

async function updateUsuario(req,res){
  try{
    var id=req.params.idUsuario;

    var usuario=new Usuario({
      usuario:req.body.usuario,
      password:req.body.password,
      status:req.body.status,
      restaurante:req.usuario.restaurante
    });

    var modificar= await Usuario.findByIdAndUpate(id,usuario);
    res.json(modificar);
    

  }catch(err){
    res.status(500).json(err);
  }

}