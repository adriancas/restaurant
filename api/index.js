var router = require('express').Router();
var passport = require('./../config/auth');
var jwt = require('jsonwebtoken');
var Usuario = require('../model/usuario.model');
var bcrypt = require('bcryptjs');
const secret = 'vextorti1125102z';

router.post('/login', function (req, res, next) {
    passport.authenticate('local', function (err, usuario) {
        if (err) return next(err);
        if (!usuario) {
            return res.status(401).json({ status: 'error', code: 'unauthorized' });
        } else {
            return res.json(jwt.sign({ _id: usuario._id }, secret));
        }
    })(req, res, next);
});

router.post('/registroUsuario',(req,res)=>{
    var usuario=new Usuario({
       usuario: 'antonio',
       rol: 'SA'
    });
    bcrypt.genSaltAsync(10)
    .then((salt) => {
        return bcrypt.hashAsync('antonio', salt);
    }).then(function (password) {
        usuario.password = password
        return usuario.save()
    })
    .then((usuario) => {
        console.log(usuario);
        res.json(usuario);
    })
})

router.use('/', require('./api'));

module.exports = router;