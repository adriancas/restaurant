var router = require('express').Router();

router.use('/sa', require('./superadmin/index'));
router.use('/admin', require('./administrador/index'));

module.exports = router;