const mongoose = require('mongoose');
const app = require('./app');
const port = process.env.PORT || 3000;
mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost:27017/restaurant',(err,res)=>{
	if(err){
		throw err;
	}else{

		console.log("Conexion exitosa");
		app.listen(port, ()=>{
			console.log("servidor corriendo en el puerto "+port);
		})
	}
});