var router = require('express').Router();
var categoriaController = require('../controllers/categoriaController');
	
	//Save Categoria
	router.post('/', categoriaController.saveCategoria);

	//Update Categoria
	router.put('/:id', categoriaController.updateCategoria);

	//Delete Categoria
	router.delete('/:id', categoriaController.deleteCategoria);

	//FindAll Categoria
	router.get('/', categoriaController.listCategoria);

	//getOne Categoria
	router.get('/:id',categoriaController.getCategoria);
	


module.exports = router;