var Proveedor = require('../../../model/proveedor.model');

async function listProveedores(req,res){
    try {
        var proveedores = await Proveedor.find({restaurante: req.usuario.restaurante});
    } catch (err) {
        res.status(500).json(err)
    }
}

async function saveProveedor(req, res) {
    try {
        var proveedor = new Proveedor({
            nombre: req.body.nombre,
            telefono: req.body.telefono,
            restaurante: req.usuario.restaurante
        });

        var saved = await proveedor.save();
        res.json(saved);
    } catch (err) {
        res.status(500).json(err);
    }
}

async function updateProveedor(req, res) {
    try {
        var id = req.params.idProveedor;
        var proveedor = new Proveedor({
            nombre: req.body.nombre,
            telefono: req.body.telefono,
            restaurante: req.usuario.restaurante
        });
        var modified = await Proveedor.findByIdAndUpdate(id,proveedor)
        res.json(modified);
    } catch (err) {
        res.status(500).json(err);
    }
}

async function deleteProveedor(req, res) {
    try {
        var id = req.params.idProveedor;
        var deleted = await Proveedor.findByIdAndRemove(id)
        res.json(deleted);
    } catch (err) {
        res.status(500).json(err);
    }
}

module.exports = {
    saveProveedor,
    updateProveedor,
    deleteProveedor,
    listProveedores
}
