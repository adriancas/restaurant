var router = require('express').Router();
var passport = require('../../config/auth');

router.all('*', function (req, res, next) {
  passport.authenticate('bearer', function (err, usuario) {
    if (err) return next(err);
    if (usuario && usuario.rol == 'SA') {
      req.usuario = usuario;
      return next();
    } else {
      return res.status(401).json({ status: 'error', code: 'unauthorized' });
    }
  })(req, res, next);
});

router.get('/', (req, res)=>{
  console.log(req.usuario);
});

module.exports = router;