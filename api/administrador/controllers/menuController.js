var Menu= require('../../../model/menu.model.js');

async function listMenu(req,res){
    try{
        var menus = await Menu.find({restaurante:req.usuario.restaurante});
        res.json(menus);

    }catch(err){res.status(err).json(err);}
}

async function saveMenu(req,res){
    try{
        var menu=new Menu({
            nombre:req.body.nombre,
            precio:req.body.precio,
            categoria:req.body.cagoria
        });

        var saved = await menu.save();
        res.json(saved);
    }catch(err){res.status(500).json(err)};
}

async function updateMenu(req,res){
    try{
        var id=req.params.idMenu;
        var menu=new Menu({
            nombre:req.body.nombre,
            precio:req.body.precio,
            categoria:req.body.cagoria
        });

        var modify= await Menu.findByIdAndUpdate(id,menu);
        res.json(modify);

    }catch(err){res.status(500)}
}

async function deleteMenu(req,res){
    try{
        var id=req.params.idMenu;
        var deleted= await Menu.findByeIdAndRemove(id);
        res.json(deleted);
    }catch(err){res.status(500).json(err)}
}

module.exports={
    listMenu,
    saveMenu,
    updateMenu,
    deleteMenu
}