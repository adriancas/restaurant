var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var adquisicion_schema = new Schema({
    producto: {type:String},
    cantidad: {type:Number},
    tipoCantidad: {type:String},
    inversion: {type:Number},
    marca: {type:String},
    fecha: {type:Date},
    descripcion: {type:String},
    restaurante: {type:Schema.Types.ObjectId,ref:"Restaurantes"},
    proveedor: {type:Schema.Types.ObjectId,ref:"Proveedores"}
});

module.exports = mongoose.model("Adquisiciones", adquisicion_schema);