var router = require('express').Router();
var mesaController =require('../controllers/mesaController');

//save
router.post('/',mesaController.saveMesa);
//Update
router.put('/:idMesa',mesaController.updateMesa);
//delete
router.delete('/:idMesa',mesaController.deleteMesa);
//findAll
router.find('/',mesaController.listMesas);

module.exports=router;