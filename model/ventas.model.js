var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var venta_schema = new Schema({
    fecha: {type: Date, default: new Date()},
    ordenes: { type: Schema.Types.ObjectId, ref: "Ordenes" },
    total: { type: Number }
});

module.exports = mongoose.model("Ventas", venta_schema);