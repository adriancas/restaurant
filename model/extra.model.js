var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var extra_schema=new Schema=({
  nombre:{type:String},
  precio:{type:Number},
  restaurante:{type:Schema.Types.ObjectId,ref:"Restaurantes"}
});
module.exports = mongoose.model("Extras", extra_schema);
