var Inventario = require('../../../model/inventario.model');

async function saveInventario(req,res){

	try{

		var inventario = new Inventario({
			producto: req.body.producto,
			cantidad: req.body.cantidad,
			tipo: req.body.tipo,
			marca: req.body.marca,
			descripcion: req.body.descripcion,
			restaurante: req.usuario.restaurante,
			categoria: req.body.categoria
		});

		var saved = await inventario.save();
        res.json(saved);

    } catch (err) {
        res.status(500).json(err);
    }

}

async function updateInventario(req,res){

	var id = req.params.id;
	try{

		var inventario = new Inventario({
			producto: req.body.producto,
			cantidad: req.body.cantidad,
			tipo: req.body.tipo,
			marca: req.body.marca,
			descripcion: req.body.descripcion,
			restaurante: req.usuario.restaurante,
			categoria: req.body.categoria
		});

	 	var modified = await Inventario.findByIdAndUpdate(id,inventario);
        res.json(modified);

    } catch (err) {
        res.status(500).json(err);
    }

}

async function deleteInventario(req,res){

	var id = req.params.id;

	try{

		var deleted = await Inventario.findByIdAndRemove(id);
        res.json(deleted);
	
	} catch (err) {
        res.status(500).json(err);
    }
}

async function listInventario(req,res){

	try {
        
        var inventarios = await Inventario.find({restaurante: req.usuario.restaurante});
        res.json(inventarios);

    } catch (err) {
        res.status(500).json(err)
    }
}

async function getInventario(req,res){

	var id = req.params.id;

	try{

		var inventario = await Inventario.findById(id).populate({path: 'categoria'}).exec();
		res.json(inventario);

	} catch (err) {
        res.status(500).json(err)
    }

}

module.exports = {
	saveInventario,
	updateInventario,
	deleteInventario,
	listInventario,
	getInventario
}