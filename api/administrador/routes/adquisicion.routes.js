var router = require('express').Router();
var AdqController = require('../controllers/adquisicionController');
//Save 
router.post('/', AdqController.saveAdquisicion);

//Update
router.put('/:idAdquisicion', AdqController.updateAdquisicion);

//FindAll
router.get('/', AdqController.listAdquisiciones);

//Delete
router.delete('/:idAdquisicion', AdqController.deleteAdquisicion);


module.exports = router;