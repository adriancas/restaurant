var mongoose = require("mongoose");
var Schema = require("mongoose").Schema;
var direccion_schema = require("./direccion.schema");

var restaurant_schema = new Schema({
    nombre: { type: String },
    razonSocial: { type: String },
    direccion: direccion_schemas
});

module.exports = mongoose.model("Restaurantes", restaurant_schema);