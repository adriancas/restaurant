var Adquisicion = require('../../../model/adiquisicion.model');

async function listAdquisiciones(req,res){
    try {
        var adquisiciones = await Adquisicion.find({restaurante: req.usuario.restaurante});
    } catch (err) {
        res.status(500).json(err)
    }
}

async function saveAdquisicion(req, res) {
    try {
        var adquisicion = new Adquisicion({
            producto: req.body.producto,
            cantidad: req.body.cantidad,
            tipoCantidad: req.body.tipoCantidad,
            inversion: req.body.inversion,
            marca: req.body.marca,
            fecha: req.body.fecha,
            descripción: req.body.descripcion,
            restaurante: req.usuario.restaurante,
            proveedor: req.body.proveedor
        });

        var saved = await adquisicion.save();
        res.json(saved);
    } catch (err) {
        res.status(500).json(err);
    }
}

async function updateAdquisicion(req, res) {
    try {
        var id = req.params.idAdquisicion;
        var adquisicion = new Adquisicion({
            producto: req.body.producto,
            cantidad: req.body.cantidad,
            tipoCantidad: req.body.tipoCantidad,
            inversion: req.body.inversion,
            marca: req.body.marca,
            fecha: req.body.fecha,
            descripción: req.body.descripcion,
            restaurante: req.usuario.restaurante,
            proveedor: req.body.proveedor
        });
        var modified = await Adquisicion.findByIdAndUpdate(id,adquisicion)
        res.json(modified);
    } catch (err) {
        res.status(500).json(err);
    }
}

async function deleteAdquisicion(req, res) {
    try {
        var id = req.params.idAdquisicion;
        var deleted = await Adquisicion.findByIdAndRemove(id)
        res.json(deleted);
    } catch (err) {
        res.status(500).json(err);
    }
}

module.exports = {
   saveAdquisicion,
   deleteAdquisicion,
   listAdquisiciones,
   updateAdquisicion
}
