var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var inventario_schema = new Schema({
	producto: { type: String },
	cantidad: { type: Number },
	tipo: { type: String },
	marca: { type: String },
	descripcion: { type: String },
	restaurante: {type:Schema.Types.ObjectId,ref:"Restaurantes"},
	categoria:{ type: Schema.Types.ObjectId, ref: "Categorias"}
});

module.exports = mongoose.model("Inventarios", inventario_schema);