var Restaurante = require("../../../model/restaurant.model");

async function saveResutaurante(req, res) {
    try {
        var restaurante = new Restaurante({
            nombre: req.body.nombre,
            razonSocial: req.body.razonSocial
        });

        var saved = await restaurante.save();
        res.json(saved);

    } catch (error) {
        res.status(500).json(error);
    }
}

module.exports = {
    saveResutaurante
}