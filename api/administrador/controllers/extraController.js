var Extra = require('../../../model/extra.model.js');

async function listExtras(req,res){
    try{
        var extra= await Extra.find({restaurante: req.usuario.restaurante});
    }catch(err){res.satatus(500).json(err)}
}

async function saveExtra(req,res){
    try{
        var extra=new Extra({
            nombre:req.body.nombre,
            precio:req.body.precio,
            restaurante:req.usuario.restaurante
        });

        var saved = await extra.save();
        res.json(saved);
    }catch(err){res.status(500).json(err)}
}

async function updateExtra(req,res){
    try{
        var id=req.params.idExtra;
        var extra=new Extra({
            nombre:req.body.nombre,
            precio:req.body.precio,
            restaurante:req.usuario.restaurante
        });

        var modified = await Extra.findByIdAndUpdate(id,extra);
        res.json(modified);
        
    }catch(err){res.status(500).json(err)}

}

async function deleteExtra(){
    try{
        var id=req.params.idExtra;
        var deleted = await Extra.findByIdAndRemove(id);
        res.json(deleted);

    }catch(err){res.status(500).json(err)}
}

module.exports={
    listExtras,
    saveExtra,
    updateExtra,
    deleteExtra
}