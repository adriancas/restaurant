var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var usuario_schema = new Schema({
    nombre: { type: String },
    apellidos: { type: String },
    telefono: { type: String },
    correo_electronico: { type: String },
    usuario: { type: String },
    password: { type: String },
    rol: { type: String },
    status: { type: Boolean, default: true },
    restaurante: { type: Schema.Types.ObjectId, ref: "Restaurantes" },
    //SI ES UN AREA (COCINA, BARRA) lleva su nombre y las categorias que pueden mostrar, si el arreglo esta vacio muestra todo. 
    nombre_area : { type: String },
    categorias: [{ type: Schema.Types.ObjectId,  ref: "Categorias"}]
});

module.exports = mongoose.model("Usuarios", usuario_schema);
