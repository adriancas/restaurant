var router = require('express').Router();
var passport = require('../../config/auth');

router.all('*', function (req, res, next) {
  passport.authenticate('bearer', function (err, usuario) {
    if (err) return next(err);
    if (usuario && usuario.rol == 'ADMIN') {
      req.usuario = usuario;
      return next();
    } else {
      return res.status(401).json({ status: 'error', code: 'unauthorized' });
    }
  })(req, res, next);
});


router.use('/proveedor', require('./routes/proveedor.routes'));
router.use('/adquisicion', require('./routes/adquisicion.routes'));
router.use('/inventario', require('./routes/inventario.routes'));
router.use('/categoria',require('./routes/categoria.routes'));



module.exports = router;