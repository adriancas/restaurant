var router = require('express').Router();
var ProvController = require('../controllers/proveedorController');
//Save Proveedor
router.post('/', ProvController.saveProveedor);

//Update Proveedor
router.put('/:idProveedor', ProvController.updateProveedor);

//FindAll Artist
router.get('/', ProvController.listProveedores);

//Delete Artist
router.delete('/:idProveedor', ProvController.deleteProveedor);


module.exports = router;