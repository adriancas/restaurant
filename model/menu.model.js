var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var menu_schema=new Schema=({
  nombre:{type:String},
  precio:{type:Number},
  categoria:{type:Schema.Types.ObjectId,ref:"Categorias"},
  img:[{type:String}],
  restaurante:{type:Schema.Types.ObjectId,ref:"Restaurantes"}
});
module.exports = mongoose.model("Menus", menu_schema);
