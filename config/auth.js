var bcrypt = require('bcryptjs');
var passport = require('passport'), LocalStrategy = require('passport-local').Strategy, BearerStrategy = require('passport-http-bearer').Strategy;
var Promise = require('bluebird');
var sanitizer = require('sanitizer');
var Usuario = require('../model/usuario.model');

var jwt = require('jsonwebtoken')

Promise.promisifyAll(bcrypt);

const secret = 'vextorti1125102z'

passport.use(new LocalStrategy({
    usernameField: 'usuario',
    passwordField: 'password',
    session: false
},
    async function (usuarioTxt, passwordTxt, done) {
        var usuarioTxt = sanitizer.sanitize(usuarioTxt);
        var usuario = await Usuario.findOne({ usuario: usuarioTxt });
        if (!usuario) { return done(null, false) };
        bcrypt.compare(passwordTxt, usuario.password, function (err, isOk) {
            isOk ? done(null, usuario) : done(null, false)
        })
    }
));

passport.use(new BearerStrategy(function (token, cb) {
    jwt.verify(token, secret, function (err, decoded) {
        if (err) return cb(err);
        Usuario.findById(decoded)
            .then(usuario => usuario ? cb(null, usuario) : cb(null, false))
            .catch(err => cb(err))
    });
}));

module.exports = passport;