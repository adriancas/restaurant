var Categoria = require('../../../model/categoria.model');

async function saveCategoria(req,res){
	try{

		var categoria = new Categoria({
			nombre: req.body.nombre,
			descripcion: req.body.descripcion,
			restaurante: req.usuario.restaurante
		});

		var saved = await categoria.save();
        res.json(saved);

	}catch(err){
		res.status(500).json(err);
	}
}

async function updateCategoria(req,res){

	var id = req.params.id;

	try{

		var categoria = new Categoria({
			nombre: req.body.nombre,
			descripcion: req.body.descripcion,
			restaurante: req.usuario.restaurante
		});

		var modified = await Categoria.findByIdAndUpdate(id,categoria);
        res.json(modified);

	}catch(err){
		res.status(500).json(err);
	}

}

async function deleteCategoria(req,res){

	var id = req.params.id;

	try{
		var deleted = await Categoria.findByIdAndRemove(id);
        res.json(deleted);
	}catch(err){
		res.status(500).json(err);
	}


}

async function listCategoria(req,res){

	try{
		
		var categorias = await Categoria.find({restaurante: req.usuario.restaurante});
        res.json(categorias);

	}catch(err){
		res.status(500).json(err);
	}


}	

async function getCategoria(req,res){

	var id = req.params.id;

	try{
		var inventario = await Inventario.findById(id);
		res.json(inventario);

	}catch(err){
		res.status(500).json(err);
	}


}

module.exports = {
	saveCategoria,
	updateCategoria,
	deleteCategoria,
	listCategoria,
	getCategoria
}