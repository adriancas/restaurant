var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var pedido_schema = new Schema({
    menu: { type: Schema.Types.ObjectId, ref: "Menus" },
    status: { type: String },
    especificaciones: String
});

var orden_schema = new Schema({
    mesa: { type: Schema.Types.ObjectId, ref: "Mesas" },
    mesero: { type: Schema.Types.ObjectId, ref: "Meseros" },
    pedidos: [pedido_schema]
});

module.exports = mongoose.model("Ordenes", orden_schema);
