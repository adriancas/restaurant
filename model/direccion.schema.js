var direccion_schema = new Schema({
    calle: { type: String },
    numero: { type: String },
    colonia: { type: String },
    municipio: { type: String },
    estado: { type: String }
});

module.exports = direccion_schema;