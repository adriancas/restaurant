var Mesa=require('../../../model/mesa.model.js');

async function listMesas (req,res){
    try{
        var mesas= await Mesa.find({restaurante: req.usuario.restaurante});
        res.json(mesas);
    }catch(err){res.status(500).json(err);}
}


async function saveMesa(req,res){
    
    try{
        var mesa=new Mesa({
            numero:req.body.numero,
            capacidad:req.body.capacidad,
            capacidadMaxima:req.body,
            restaurante:req.usuario.restaurante
        });
        
        var saved= await mesa.save();
        res.json(saved);
    }catch(err){res.status(500).json(err);}
}

async function updateMesa(req,res){
    try{
        var id=req.params.idMesa;

        var mesa=new Mesa({
            numero:req.body.numero,
            capacidad:req.body.capacidad,
            capacidadMaxima:req.body,
            restaurante:req.usuario.restaurante
        });

        var modified= await Mesa.findByIdAndUpdate(id,mesa);
        res.json(modified);


    }catch(err){res.status(500).json(err);}
}

async function deleteMesa(req,res){
    try{
        var id=req.params.idMesa;
        var deleted = await Mesa.findByIdAndRemove(id);
        res.json(deleted);
    }catch(err){res.status(err).json(err);}

}


module.exports={
    listMesas,
    saveMesa,
    updateMesa,
    deleteMesa
}